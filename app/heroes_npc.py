import random

from heroes import Heroes
from heroes_type import HeroesType
from heroes_by_type import heroes_by_type
from body_part import BodyPart


class HeroesNpc(Heroes):
    def __init__(self):
        random_type_value = random.randint(HeroesType.min_value(), HeroesType.max_value())
        random_heroes_type = HeroesType(random_type_value)
        random_heroes_name = random.choice(list(heroes_by_type.get(random_heroes_type, {}).keys()))

        super().__init__(random_heroes_name, random_heroes_type)

    def next_step_points(self, **kwargs):
        attack_point = BodyPart(random.randint(BodyPart.min_value(), BodyPart.max_value()))
        defence_point = BodyPart(random.randint(BodyPart.min_value(), BodyPart.max_value()))
        super().next_step_points(next_attack=attack_point,
                                 next_defence=defence_point)


if __name__ == '__main__':
    heroes_npc = HeroesNpc()
    heroes_npc.next_step_points()
    print(heroes_npc)
