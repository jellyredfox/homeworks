import random
from combat.heroes_type import HeroesType
from combat.heroes_types_weaknesses import heroes_defence_weakness_by_type as weakness_by_type
from combat.body_part import BodyPart
from combat.heroes_state import HeroesState


class Heroes:
    def __init__(self, name, heroes_type: HeroesType):
        self.name = name
        self.heroes_type = heroes_type
        self.weakness = weakness_by_type.get(heroes_type, tuple())
        self.hp = 100
        self.attack_point = None
        self.defence_point = None
        self.hit_power = 10
        self.state = HeroesState.READY

    def __str__(self):
        return f"Name: {self.name} | Type: {self.heroes_type.name}\nHP: {self.hp}"

    def next_step_points(self, next_attack: BodyPart,
                         next_defence: BodyPart):
        self.attack_point = next_attack
        self.defence_point = next_defence

    def get_hit(self,
                opponent_attack_point: BodyPart,
                opponent_hit_power: int,
                opponent_type: HeroesType):
        if opponent_attack_point == BodyPart.NOTHING:
            return random.choice(['Пропустить ход? Глупое решение!', 'Спасибо что поддаешься',
                                  'Продолжай и дальше ничего не делать'])
        elif self.defence_point == opponent_attack_point:
            return random.choice(['Ха! Отбил.', 'Твои удары способен отразить даже младенец',
                                  'Так ты меня никогда не одолеешь'])
        elif self.hp > 0:
            self.hp -= opponent_hit_power * (1.5 if opponent_type in self.weakness else 1)
            if self.hp <= 0:
                self.state = HeroesState.DEFEATED
                return f'{self.name} побежден'
            return random.choice(['Ох, больно!', 'Ты поплатишься за это!', 'Я запомнил!',
                                  'Моя месть будет страшна!'])
