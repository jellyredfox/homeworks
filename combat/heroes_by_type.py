from combat.heroes_type import HeroesType

heroes_by_type = {
    HeroesType.TANK: {'Кэрн Кровавое копыто': 'кэрн.jpg',
                      'Магни Бронзобород': 'магни.jpg',
                      'Малфурион': 'малфурион.jpg'},

    HeroesType.RANGE: {'Джайна': 'джайна.jpg',
                       'Рексар': 'рэксар.jpg',
                       'Гулдан': 'гулдан.jpg'},

    HeroesType.MELEE: {'Валира': 'валира.jpg',
                       'Иллидан': 'иллидан.jpg',
                       'Эдвин ван Клифф': 'ванклиф.jpg'},

    HeroesType.HEAL: {'Андуин': 'андуин.jpg',
                      'Утер': 'утер.jpg',
                      'Тралл': 'тралл.jpg'}

}