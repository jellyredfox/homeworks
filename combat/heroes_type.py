from enum import Enum, auto


class HeroesType(Enum):
    TANK = auto()
    RANGE = auto()
    MELEE = auto()
    HEAL = auto()

    @classmethod
    def min_value(cls):
        return cls.TANK.value

    @classmethod
    def max_value(cls):
        return cls.HEAL.value

