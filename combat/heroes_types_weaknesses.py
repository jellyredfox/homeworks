from combat.heroes_type import HeroesType

heroes_defence_weakness_by_type = {

    HeroesType.TANK: (HeroesType.RANGE, HeroesType.HEAL),

    HeroesType.MELEE: (HeroesType.TANK, ),

    HeroesType.RANGE: (HeroesType.MELEE, HeroesType.HEAL),

    HeroesType.HEAL: (HeroesType.MELEE, )

}
