from enum import Enum, auto


class HeroesState(Enum):
    READY = auto()
    DEFEATED = auto()
