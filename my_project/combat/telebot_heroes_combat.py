import json

import telebot
from telebot import types
from telebot.types import Message

from app.combat.body_part import BodyPart
from app.combat.game_result import GameResult
from app.combat.heroes import Heroes
from app.combat.heroes_npc import HeroesNpc
from app.combat.heroes_by_type import heroes_by_type
from app.combat.heroes_state import HeroesState
from app.combat.heroes_type import HeroesType

with open('./bot_token.txt', 'r') as token_file:
    token = token_file.read().strip()

bot = telebot.TeleBot(token)

state = {}

statistics = {}

stat_file = 'game_stat.json'

body_part_keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True,
                                               one_time_keyboard=True,
                                               row_width=len(BodyPart))

body_part_keyboard.row(*[types.KeyboardButton(body_part.name) for body_part in BodyPart])


def update_save_stat(chat_id, result: GameResult):
    print('Обновление статистики', end='...')
    global statistics

    chat_id = str(chat_id)

    if statistics.get(chat_id, None) is None:
        statistics[chat_id] = {}

    if result == GameResult.W:
        statistics[chat_id]['W'] = statistics[chat_id].get('W', 0) + 1
    elif result == GameResult.L:
        statistics[chat_id]['L'] = statistics[chat_id].get('L', 0) + 1
    elif result == GameResult.E:
        statistics[chat_id]['E'] = statistics[chat_id].get('E', 0) + 1
    else:
        print(f'Не существует результата {result}')

    with open(stat_file, 'w') as file:
        json.dump(statistics, file)

    print('Завершено!')


def load_stat():
    print('Загрузка статистики', end='...')
    global statistics

    try:
        with open(stat_file, 'r') as file:
            statistics = json.load(file)
        print('Завершена успешно!')
    except FileNotFoundError:
        statistics = {}
        print('Файл не найден!')


@bot.message_handler(commands=['help', 'info'])
def help_command(message):
    bot.send_message(message.chat.id,
                     'Hi!\n/start для начала игры\n/stat для отображения статистики')


@bot.message_handler(commands=['stat'])
def stat(message):
    global statistics
    if statistics.get(str(message.chat.id), None) is None:
        user_stat = 'Еще не было ни одной игры'
    else:
        user_stat = 'Твои результаты:'
        for res, num in statistics[str(message.chat.id)].items():
            user_stat += f'\n{res}: {num}'

    bot.send_message(message.chat.id,
                     text=user_stat)


@bot.message_handler(commands=['start'])
def start(message):
    yes_no_keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True,
                                                one_time_keyboard=True,
                                                row_width=2)
    yes_no_keyboard.row('Да', 'Нет')

    bot.send_message(message.chat.id,
                     text='Готов начать битву героев?',
                     reply_markup=yes_no_keyboard)

    bot.register_next_step_handler(message, start_question_handler)


def start_question_handler(message):
    if message.text.lower() == 'да':
        bot.send_message(message.chat.id,
                         'Хорошо, начинаем!')

        create_npc(message)

        ask_user_about_heroes_type(message)

    elif message.text.lower() == 'нет':
        bot.send_message(message.chat.id,
                         'Ок, я подожду.')
    else:
        bot.send_message(message.chat.id,
                         'Я не знаю такого варианта ответа!')


def create_npc(message):
    print(f'Начало создания объекта NPC для chat id = {message.chat.id}')
    global state
    heroes_npc = HeroesNpc()
    if state.get(message.chat.id, None) is None:
        state[message.chat.id] = {}
        state[message.chat.id]['npc_heroes'] = heroes_npc

    npc_image_filename = heroes_by_type[heroes_npc.heroes_type][heroes_npc.name]
    bot.send_message(message.chat.id, 'Противник:')
    with open(f'images/{npc_image_filename}', 'rb') as file:
        bot.send_photo(message.chat.id, file, heroes_npc)
        print(f'Завершено создание объекта NPC для chat id = {message.chat.id}')


def ask_user_about_heroes_type(message):
    markup = types.InlineKeyboardMarkup()

    for heroes_type in HeroesType:
        markup.add(types.InlineKeyboardButton(text=heroes_type.name,
                                              callback_data=f'heroes_type_{heroes_type.value}'))

    bot.send_message(message.chat.id, 'Выбери тип героя:', reply_markup=markup)


@bot.callback_query_handler(func=lambda call: 'heroes_type_' in call.data)
def heroes_type_handler(call):
    call_data_split = call.data.split('_')
    if len(call_data_split) != 3 or not call_data_split[2].isdigit():
        bot.send_message(call.message.chat.id, 'Возникла проблема. Перезапусти сессию!')
    else:
        heroes_type_id = int(call_data_split[2])

        bot.send_message(call.message.chat.id, 'Выбери своего героя:')

        ask_user_about_heroes_by_type(heroes_type_id, call.message)


def ask_user_about_heroes_by_type(heroes_type_id, message):
    heroes_type = HeroesType(heroes_type_id)
    heroes_dict_by_type = heroes_by_type.get(heroes_type, {})

    for heroes_name, heroes_img in heroes_dict_by_type.items():
        markup = types.InlineKeyboardMarkup()
        markup.add(types.InlineKeyboardButton(text=heroes_name,
                                              callback_data=f'heroes_name_{heroes_type_id}_{heroes_name}'))
        with open(f'images/{heroes_img}', 'rb') as file:
            bot.send_photo(message.chat.id, file, reply_markup=markup)


@bot.callback_query_handler(func=lambda call: 'heroes_name_' in call.data)
def heroes_name_handler(call):
    call_data_split = call.data.split('_')
    if len(call_data_split) < 4 or not call_data_split[2].isdigit():
        bot.send_message(call.message.chat.id, 'Возникла проблема. Перезапусти сессию!')
    else:
        heroes_type_id, heroes_name = int(call_data_split[2]), call_data_split[3]

        create_user_heroes(call.message, heroes_type_id, heroes_name)

        bot.send_message(call.message.chat.id, 'Игра началась!')

        game_next_step(call.message)


def create_user_heroes(message, heroes_type_id, heroes_name):
    print(f'Начало создания объекта Heroes для chat_id = {message.chat.id} ')
    global state
    user_heroes = Heroes(name=heroes_name,
                         heroes_type=HeroesType(heroes_type_id))

    if state.get(message.chat.id, None) is None:
        state[message.chat.id] = {}
    state[message.chat.id]['user_heroes'] = user_heroes

    image_filename = heroes_by_type[user_heroes.heroes_type][user_heroes.name]
    bot.send_message(message.chat.id, 'Твой выбор:')
    with open(f'images/{image_filename}', 'rb') as file:
        bot.send_photo(message.chat.id, file, user_heroes)

    print(f'Завершено создание объекта Heroes для chat id = {message.chat.id}')


def game_next_step(message: Message):
    bot.send_message(message.chat.id,
                     'Выбор точки для защиты:',
                     reply_markup=body_part_keyboard)

    bot.register_next_step_handler(message, reply_defend)


def reply_defend(message: Message):
    if not BodyPart.has_item(message.text):
        bot.send_message(message.chat.id, 'Необходимо выбрать вариант на клавиатуре!')
        game_next_step(message)
    else:
        bot.send_message(message.chat.id,
                         'Выбор точки для удара:',
                         reply_markup=body_part_keyboard)

        bot.register_next_step_handler(message, reply_attack, defend_body_part=message.text)


def reply_attack(message: Message, defend_body_part: str):
    if not BodyPart.has_item(message.text):
        bot.send_message(message.chat.id, 'Необходимо выбрать вариант на клавиатуре!')
        game_next_step(message)
        attack_body_part = message.text
    else:
        attack_body_part = message.text

    global state

    user_heroes = state[message.chat.id]['user_heroes']

    heroes_npc = state[message.chat.id]['npc_heroes']

    user_heroes.next_step_points(next_attack=BodyPart[attack_body_part],
                                 next_defence=BodyPart[defend_body_part])

    heroes_npc.next_step_points()

    game_step(message, user_heroes, heroes_npc)


def game_step(message: Message, user_heroes: Heroes, heroes_npc: Heroes):
    comment_npc = heroes_npc.get_hit(opponent_attack_point=user_heroes.attack_point,
                                     opponent_hit_power=user_heroes.hit_power,
                                     opponent_type=user_heroes.heroes_type)
    image_file = heroes_by_type[heroes_npc.heroes_type][heroes_npc.name]
    with open(f'images/мини_{image_file}', 'rb') as ifile:
        bot.send_photo(message.chat.id, ifile, f'{heroes_npc.name}: \"{comment_npc}\"\nHP: {heroes_npc.hp}')
    # bot.send_message(message.chat.id, f'{heroes_npc.name}: \"{comment_npc}\"\nHP: {heroes_npc.hp}')

    comment_user = user_heroes.get_hit(opponent_attack_point=heroes_npc.attack_point,
                                       opponent_hit_power=heroes_npc.hit_power,
                                       opponent_type=heroes_npc.heroes_type)
    image_file = heroes_by_type[user_heroes.heroes_type][user_heroes.name]
    with open(f'images/мини_{image_file}', 'rb') as ifile:
        bot.send_photo(message.chat.id, ifile, f'(Ты) {user_heroes.name}: \"{comment_user}\"\nHP: {user_heroes.hp}')
    # bot.send_message(message.chat.id, f'(Ты) {user_heroes.name}: \"{comment_user}\"\nHP: {user_heroes.hp}')

    if heroes_npc.state == HeroesState.READY and user_heroes.state == HeroesState.READY:
        bot.send_message(message.chat.id, 'Продолжаем бой!')
        game_next_step(message)
    elif heroes_npc.state == HeroesState.DEFEATED and user_heroes.state == HeroesState.DEFEATED:
        bot.send_message(message.chat.id, 'Невероятно, у нас ничья!')
        update_save_stat(message.chat.id, GameResult.E)
    elif heroes_npc.state == HeroesState.DEFEATED:
        bot.send_message(message.chat.id, f'Поздравляю, {user_heroes.name}, ты победил!')
        update_save_stat(message.chat.id, GameResult.W)
    elif user_heroes.state == HeroesState.DEFEATED:
        bot.send_message(message.chat.id, f'{user_heroes.name} проиграл этот бой.')
        update_save_stat(message.chat.id, GameResult.L)
    else:
        start(message)


if __name__ == '__main__':
    load_stat()

    print('Starting the bot...')
    bot.polling(none_stop=True, interval=0)
    print('The bot has stopped!')
