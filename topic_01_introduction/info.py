"""
Ввод данных с клавиатуры:
- Имя
- Фамилия
- Возраст

Вывод данных в формате:
"Вас зовут <Имя> <Фамилия>! Ваш возраст равен <Возраст>."
"""
name = input('Имя: ')
second_name = input('Фамилия: ')
age = input('Возраст: ')

print('Вас зовут', name, second_name + '!', 'Ваш возраст равен', age + '.')
