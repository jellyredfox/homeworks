"""
Функция check_sum.

Принимает 3 числа.
Вернуть True, если можно взять какие-то два из них и в сумме получить третье, иначе False
"""


def check_sum(num1, num2, num3):
    if num1 + num2 == num3:
        return True
    elif num1 + num3 == num2:
        return True
    elif num2 + num3 == num1:
        return True
    else:
        return False
