"""
Функция sum_1_112_3.

Вернуть сумму 1+4+7+10+...109+112.
"""


def sum_1_112_3():
    sum_numb = 0
    for i in range(1, 112 + 1, 3):
        sum_numb += i
    return sum_numb
