"""
Функция count_odd_num.

Принимает натуральное число (целое число > 0).
Верните количество нечетных цифр в этом числе.
Если число меньше или равно 0, то вернуть "Must be > 0!".
Если число не целое (не int, а другой тип данных), то вернуть "Must be int!".
"""


def count_odd_num(numb):
    count = 0
    if type(numb) != int:
        return "Must be int!"
    elif numb <= 0:
        return "Must be > 0!"
    else:
        for i in str(numb):
            if int(i) % 2 != 0:
                count += 1
        return count

