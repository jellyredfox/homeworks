"""
Функция print_nth_symbols.

Принимает строку и натуральное число n (целое число > 0).
Вывести символы с индексом n, n*2, n*3 и так далее.
Пример: string='123456789qwertyuiop', n = 2 => result='3579wryip'

Если число меньше или равно 0, то вывести строку 'Must be > 0!'.
Если тип n не int, то вывести строку 'Must be int!'.

Если n больше длины строки, то вывести пустую строку.
"""


# def print_nth_symbols(string, n, result=''):
def print_nth_symbols(string, n):
    if type(n) != int:
        print('Must be int!')
    elif n <= 0:
        print('Must be > 0!')
    elif len(str(n)) > len(string):
        print("")
    else:
        print(string[n::n])

        # for i in range(1, (len(string))):
        #     if n * i <= len(string) - 1:
        #         result += string[n * i]
        # print(result)
