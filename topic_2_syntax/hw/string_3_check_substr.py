"""
Функция check_substr.

Принимает две строки.
Если меньшая по длине строка содержится в большей, то возвращает True,
иначе False.
Если строки равны, то False.
Если одна из строк пустая, то True.
"""


def check_substr(string1, string2):
    if string1 == string2:
        return False
    elif string1 == '' or string2 == '':
        return True
    elif len(string1) < len(string2) and string1 in string2:
        return True
    elif len(string2) < len(string1) and string2 in string1:
        return True
    else:
        return False
