def check_numbers_by_5(x, y, z):
    """
    Функция check_numbers_by_5.

    Принимает 3 числа.
    Если ровно два из них меньше 5, то вернуть True, иначе вернуть False.
    """
    # альтернативный вариант
    # if (x < 5 and y < 5 and z >= 5) or (x < 5 and z < 5 and y >= 5) or (y < 5 and z < 5 and x >= 5):
    #     return True
    # else:
    #     return False

    return (x < 5 and y < 5 and z >= 5) or (x < 5 and z < 5 and y >= 5) or (y < 5 and z < 5 and x >= 5)
