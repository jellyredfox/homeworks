"""
Функция print_welcome.

Принимает число n.
Выведите на экран n раз фразу "You are welcome!".
Пример: n=3, тогда в результате "You are welcome!You are welcome!You are welcome!"
"""