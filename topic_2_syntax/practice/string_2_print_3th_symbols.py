def print_3th_symbols(my_str):
    """
    Функция print_3th_symbols.

    Принимает строку.
    Вывести третий, шестой, девятый и так далее символы.
    """

    print(my_str[2::3])
