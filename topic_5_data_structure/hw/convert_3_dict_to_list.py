"""
Функция dict_to_list.

Принимает 1 аргумент: словарь.

Возвращает список: [
    список ключей,
    список значений,
    количество уникальных элементов в списке ключей,
    количество уникальных элементов в списке значений
].

Если вместо словаря передано что-то другое, то возвращать строку 'Must be dict!'.

Если dict пуст, то возвращать ([], [], 0, 0).
"""


def dict_to_list(my_dict: dict):
    if type(my_dict) != dict:
        return 'Must be dict!'
    elif len(my_dict) == 0:
        return [], [], 0, 0

    key_list = []
    for k in my_dict.keys():
        key_list.append(k)

    value_list = []
    for v in my_dict.values():
        value_list.append(v)

    len_uniq_key = len(set(my_dict.keys()))
    len_uniq_value = len(set(my_dict.values()))

    return key_list, value_list, len_uniq_key, len_uniq_value
