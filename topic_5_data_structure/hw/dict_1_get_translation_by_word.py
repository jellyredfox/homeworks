"""
Функция get_translation_by_word.

Принимает 2 аргумента:
    ru-eng словарь содержащий {ru_word: [eng_1, eng_2 ...], ...}
    слово для поиска в словаре (ru).

Возвращает все варианты переводов (list), если такое слово есть в словаре,
если нет, то "Can't find Russian word: {word}".

Если вместо словаря передано что-то другое, то возвращать строку "Dictionary must be dict!".
Если вместо строки для поиска передано что-то другое, то возвращать строку "Word must be str!".

Если словарь пустой, то возвращать строку "Dictionary is empty!".
Если строка для поиска пустая, то возвращать строку "Word is empty!".
"""


def get_translation_by_word(my_dict: dict, word: str):
    if type(my_dict) != dict:
        return "Dictionary must be dict!"
    elif type(word) != str:
        return "Word must be str!"
    elif len(my_dict) == 0:
        return "Dictionary is empty!"
    elif len(word) == 0:
        return "Word is empty!"

    return my_dict.get(word, f"Can't find Russian word: {word}")
