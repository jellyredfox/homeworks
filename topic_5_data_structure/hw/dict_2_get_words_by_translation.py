"""
Функция get_words_by_translation.

Принимает 2 аргумента:
    ru-eng словарь содержащий {ru_word: [eng_1, eng_2 ...], ...}
    слово для поиска в словаре (eng).

Возвращает список всех вариантов переводов (ru), если такое слово есть в словаре (eng),
если нет, то "Can't find English word: {word}".

Если вместо словаря передано что-то другое, то возвращать строку "Dictionary must be dict!".
Если вместо строки для поиска передано что-то другое, то возвращать строку "Word must be str!".

Если словарь пустой, то возвращать строку "Dictionary is empty!".
Если строка для поиска пустая, то возвращать строку "Word is empty!".
"""


def get_words_by_translation(ru_eng_dict: dict, eng_word: str):
    if type(ru_eng_dict) != dict:
        return "Dictionary must be dict!"
    elif type(eng_word) != str:
        return "Word must be str!"
    elif len(ru_eng_dict) == 0:
        return "Dictionary is empty!"
    elif len(eng_word) == 0:
        return "Word is empty!"

    result = []
    for word, translate in list(ru_eng_dict.values()):
        if word == eng_word:
            result.append(translate)
        if result == 0:
            return f"Can't find English word: {eng_word}"
        return result
