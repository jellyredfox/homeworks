"""
Функция pow_start_stop.

Принимает числа start, stop.

Возвращает список состоящий из квадратов значений от start до stop (не включая).

Пример: start=3, stop=6, результат [9, 16, 25].

Если start или stop не являются int, то вернуть строку 'Start and Stop must be int!'.
"""


def pow_start_stop(start, stop):
    if type(start) != int or type(stop) != int:
        return 'Start and Stop must be int!'
    return [x * x for x in range(start, stop)]