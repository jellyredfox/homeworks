"""
Функция get_info_for_3_set.

Принимает 3 аргумента: множества my_set_left, my_set_mid и my_set_right.

Возвращает dict с информацией:
{
'left == mid == right': True/False,
'left == mid': True/False,
'left == right': True/False,
'mid == right': True/False,

'left & mid': set(...), # intersection
'left & right': set(...),   # intersection
'mid & right': set(...),# intersection

'left <= mid': True/False, # issubset
'mid <= left': True/False, # issubset
'left <= right': True/False,   # issubset
'right <= left': True/False,   # issubset
'mid <= right': True/False,# issubset
'right <= mid': True/False # issubset
}

Если вместо множеств передано что-то другое, то возвращать строку 'Must be set!'.
"""


def get_info_for_3_set(my_set_left, my_set_mid, my_set_right):
    if type(my_set_left) != set or type(my_set_mid) != set or type(my_set_right) != set:
        return 'Must be set!'

    return {'left == mid == right': my_set_left == my_set_mid == my_set_right,
            'left == mid': my_set_left == my_set_mid,
            'left == right': my_set_left == my_set_right,
            'mid == right': my_set_mid == my_set_right,
            'left & mid': my_set_left.intersection(my_set_mid),
            'left & right': my_set_left.intersection(my_set_right),
            'mid & right': my_set_mid.intersection(my_set_right),
            'left <= mid': my_set_left.issubset(my_set_mid),
            'mid <= left': my_set_mid.issubset(my_set_left),
            'left <= right': my_set_left.issubset(my_set_right),
            'right <= left': my_set_right.issubset(my_set_left),
            'mid <= right': my_set_mid.issubset(my_set_right),
            'right <= mid': my_set_right.issubset(my_set_mid)}
