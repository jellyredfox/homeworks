"""
Функция zip_names.

Принимает 2 аргумента: список с именами и множество с фамилиями.

Возвращает список с парами значений из каждого аргумента.

Если вместо list передано что-то другое, то возвращать строку 'First arg must be list!'.
Если вместо set передано что-то другое, то возвращать строку 'Second arg must be set!'.

Если list пуст, то возвращать строку 'Empty list!'.
Если set пуст, то возвращать строку 'Empty set!'.

Если list и set различного размера, обрезаем до минимального (стандартный zip).
"""


def zip_names(names_list: list, last_names_set: set):
    if type(names_list) != list:
        return 'First arg must be list!'
    elif type(last_names_set) != set:
        return 'Second arg must be set!'
    elif not names_list:
        return 'Empty list!'
    elif not last_names_set:
        return 'Empty set!'

    return list(zip(last_names_set, names_list))
