"""
Функция zip_colour_shape.

Принимает 2 аргумента: список с цветами(зеленый, красный) и кортеж с формами (квадрат, круг).

Возвращает список с парами значений из каждого аргумента.

Если вместо list передано что-то другое, то возвращать строку 'First arg must be list!'.
Если вместо tuple передано что-то другое, то возвращать строку 'Second arg must be tuple!'.

Если list пуст, то возвращать строку 'Empty list!'.
Если tuple пуст, то возвращать строку 'Empty tuple!'.

Если list и tuple различного размера, обрезаем до минимального (стандартный zip).
"""

def zip_colour_shape(colors_list: list, forms_tuple: tuple):
    if type(colors_list) != list:
        return 'First arg must be list!'
    elif type(forms_tuple) != tuple:
        return 'Second arg must be tuple!'
    elif not colors_list:
        return 'Empty list!'
    elif not forms_tuple:
        return 'Empty tuple!'

    return list(zip(colors_list, forms_tuple))
