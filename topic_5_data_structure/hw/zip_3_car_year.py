"""
Функция zip_car_year.

Принимает 2 аргумента: список с машинами и список с годами производства.

Возвращает список с парами значений из каждого аргумента, если один список больше другого,
то заполнить недостающие элементы строкой "???".

Подсказка: zip_longest.

Если вместо списков передано что-то другое, то возвращать строку 'Must be list!'.
Если список (хотя бы один) пуст, то возвращать строку 'Empty list!'.
"""
from itertools import zip_longest


def zip_car_year(cars_list: list, years_list):
    if type(cars_list) != list or type(years_list) != list:
        return 'Must be list!'
    elif not cars_list or not years_list:
        return 'Empty list!'

    return list(zip_longest(cars_list, years_list, fillvalue="???"))
