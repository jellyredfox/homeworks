def set_to_dict(my_set, val):
    """
    Функция set_to_dict.

    Принимает 2 аргумента: множество и значение для поиска.

    Возвращает словарь (из входного множества), в котором
        ключ = значение из множества,
        значение = (
        равен ли элемент из множества значению для поиска (True | False),
        количество элементов в словаре
    )

    Если вместо множества передано что-то другое, то возвращать строку 'First arg must be set!'.

    Если множество пусто, то возвращать пустой словарь.

    ВНИМАНИЕ: при повторяющихся ключах в dict записывается значение последнего добавленного ключа.

    ВНИМАНИЕ: нумерация индексов начинается с 0!
    """

    if type(my_set) != set:
        return 'First arg must be set!'

    if len(my_set) == 0:
        return {}

    my_dict = {}
    dict_len = len(my_set)
    for item in my_set:
        my_dict[item] = (item == val, dict_len)

    return my_dict
