def get_numbers_by_name(names_with_phones, word):
    """
    Функция get_numbers_by_name.

    Принимает 2 аргумента:
        словарь содержащий {имя: [телефон1, телефон2], ...},
        слово (имя) для поиска в словаре.

    Если вместо словаря передано что-то другое, то возвращать строку "Dictionary must be dict!".
    Если вместо строки для поиска передано что-то другое, то возвращать строку "Name must be str!".

    Если словарь пустой, то возвращать строку "Dictionary is empty!".
    Если строка для поиска пустая, то возвращать строку "Name is empty!".

    Вернуть все телефоны по имени (list), если такое имя есть в словаре,
    если нет, то "Can't find name: {name}".
    """

    if type(names_with_phones) != dict:
        return "Dictionary must be dict!"
    if type(word) != str:
        return "Name must be str!"

    if len(names_with_phones) == 0:
        return "Dictionary is empty!"

    if len(word) == 0:
        return "Name is empty!"

    # Marina
    # for item in names_with_phones:
    #     if item == word:
    #         return names_with_phones.get(item)
    #
    # return f"Can't find name: {word}"

    # Nikolay
    # if word in names_with_phones:
    #     return names_with_phones[word]
    # else:
    #     return f"Can't find name: {word}"

    return names_with_phones.get(word, f"Can't find name: {word}")


if __name__ == '__main__':
    get_numbers_by_name({"ira": ["1-2-3"],
                         "ivan": ["33-44-55", "99-3-1"]},
                        "ivan")
