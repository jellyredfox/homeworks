# Sasha Y
def get_name_by_number(names_with_phones: dict, phone: str):
    """
    Функция get_name_by_number.

    Принимает 2 аргумента:
        словарь содержащий {имя: [телефон1, телефон2, ...], ...},
        номер телефона (строка) для поиска в словаре.

    Возвращает имя владельцев телефона (список), если такой номер есть в словаре,
    если нет, то "Can't find phone: {phone}".

    Если вместо словаря передано что-то другое, то возвращать строку "Dictionary must be dict!".
    Если вместо строки для поиска передано что-то другое, то возвращать строку "Phone number must be str!".

    Если словарь пустой, то возвращать строку "Dictionary is empty!".
    Если строка для поиска пустая, то возвращать строку "Phone number is empty!".
    """

    if type(names_with_phones) != dict:
        return "Dictionary must be dict!"
    if type(phone) != str:
        return "Phone number must be str!"

    if len(names_with_phones) == 0:
        return "Dictionary is empty!"

    if len(phone) == 0:
        return "Phone number is empty!"

    users = []

    for name, phone_list in names_with_phones.items():
        if phone in phone_list:
            users.append(name)

    if len(users) == 0:
        return f"Can't find phone: {phone}"

    return users


if __name__ == '__main__':
    get_name_by_number({"ira": ["1-2-3"],
                        "ivan": ["33-44-55", "99-3-1"]},
                       "1-2-3")
