# Коля
def double_start_stop(start, stop):
    """
    Функция double_start_stop.

    Принимает 2 аргумента: числа start, stop.

    Возвращает словарь, в котором
    ключ - это значение от start до stop (не включая),
    значение - удвоенных значение ключа.

    Пример: start=3, stop=6, результат {3: 6, 4: 8, 5: 10}.

    Если start или stop не являются int,
    то вернуть строку 'Start and Stop must be int!'.
    """

    if not isinstance(start, int) or not isinstance(stop, int):
        return 'Start and Stop must be int!'

    return {val: val * 2 for val in range(start, stop)}
