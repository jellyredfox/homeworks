# Саша Я
import itertools


def pow3_start_inf_step(start, step):
    """
    Функция pow3_start_inf_step.

    Принимает 2 аргумента: число start, step.

    Возвращает генератор-выражение состоящий из
    значений в 3 степени от start до бесконечности с шагом step.

    Пример: start=3, step=2 результат 3^3, 5^3, 7^3, 9^3 ... (infinity).

    Если start или step не являются int,
    то вернуть строку 'Start and Step must be int!'.
    """

   # if int not in (type(start), type(step)):# НЕ СРАБОТАЕТ В ДАННОМ ЗАДАНИИ

    # if not isinstance(start, int) or not isinstance(step, int):
    # if (type(start), type(step)) != (int, int):
    if not (type(start) == type(step) == int):
        return 'Start and Step must be int!'

    return (x ** 3 for x in itertools.count(start, step))


if __name__ == '__main__':
    pow3_start_inf_step("2", 4)
