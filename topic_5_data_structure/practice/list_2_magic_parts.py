# Sasha Y
def magic_parts(my_list):
    """
    Функция magic_parts.

    Принимает 1 аргумент: список my_list.

    Возвращает список, который состоит из
    [первые 2 элемента my_list]
    + [последний элемент my_list]
    + [количество элементов в списке my_list].

    Пример: входной список [1, 2, ‘aa’, ‘mm’], результат [1, 2, ‘mm’, 4].

    Если вместо списка передано что-то другое, то возвращать строку 'Must be list!'.
    Если список пуст, то возвращать строку 'Empty list!'.
    """

    if type(my_list) != list:
        return 'Must be list!'
    elif len(my_list) == 0:
        return 'Empty list!'

    # return my_list[0:2] + [my_list[-1]] + [len(my_list)]
    return my_list[0:2] + [my_list[-1], len(my_list)]


if __name__ == '__main__':
    print(magic_parts(list()))
    print(magic_parts(444))
    print(magic_parts([1, 2, 'aa', 'mm']))
    print(magic_parts([1, 2, 'mm']))
    print(magic_parts([1, 'mm']))
    print(magic_parts([1]))
