from itertools import zip_longest


def zip_name_mark(names: list, marks: list):
    """
    Функция zip_name_mark.

    Принимает 2 аргумента: список с именами и список с баллами.

    Возвращает список (list) с парами значений из каждого аргумента, если один список больше другого,
    то заполнить недостающие элементы строкой "!!!".

    Подсказка: zip_longest.

    Если вместо списков передано что-то другое или список (хотя бы один) пуст,
    то возвращать строку 'Both args must be list and not empty!'.

    Если в списке с именами есть элементы НЕ str, то возвращать строку 'Names must be str!'

    Если в списке с оценками есть элементы НЕ int (0 <= mark <= 100),
    то возвращать строку 'Marks must be int (0 to 100)!'
    """

    if not isinstance(names, list) or not isinstance(marks, list) or not names or not marks:
        return 'Both args must be list and not empty!'

    for name in names:
        if not isinstance(name, str):
            return 'Names must be str!'

    for mark in marks:
        if not isinstance(mark, int) or not 0 <= mark <= 100:
            return 'Marks must be int (0 to 100)!'

    return list(zip_longest(names, marks, fillvalue="!!!"))
