"""
Функция save_dict_to_file_classic.

Принимает 2 аргумента: строка (название файла или полный путь к файлу), словарь (для сохранения).

Сохраняет список в файл. Проверить, что записалось в файл.
"""


def save_dict_to_file_classic(path: str, my_dict: dict):
    with open(path, 'w+') as file:
        my_dict_write = file.write(str(my_dict))


if __name__ == '__main__':
    file_path = 'test.txt'
    save_dict_to_file_classic(file_path, {1: 2, 2: 3, 3: 4})

    with open(file_path, 'r') as f:
        my_dict_str = f.read()
        print(type(my_dict_str), my_dict_str)
