"""
Функция save_dict_to_file_pickle.

Принимает 2 аргумента: строка (название файла или полный путь к файлу), словарь (для сохранения).

Сохраняет список в файл. Загрузить словарь и проверить его на корректность.
"""
import pickle


def save_dict_to_file_pickle(my_file: str, my_dict: dict):
    with open(my_file, 'wb') as pic_file:
        pickle.dump(my_dict, pic_file)

    file = open(my_file, 'rb')
    save_dict = pickle.load(file)
    file.close()

    print(f"my_dict == save_dict: {my_dict == save_dict}")
    print(f"type(save_dict): {type(save_dict)}")


if __name__ == '__main__':
    save_dict_to_file_pickle('test2.txt', {1: 2, 2: 3, 3: 4})
