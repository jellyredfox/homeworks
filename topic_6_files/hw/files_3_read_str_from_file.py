"""
Функция read_str_from_file.

Принимает 1 аргумент: строка (название файла или полный путь к файлу).

Выводит содержимое файла (в консоль). (* Файл должен быть предварительно создан и наполнен каким-то текстом.)
"""


def read_str_from_file(file_path: str):
    with open(file_path, 'r') as file:
        my_file_for_read = file.read()
        print(my_file_for_read)


if __name__ == '__main__':
    read_str_from_file('test.txt')
