# Коля
def save_list_to_file_classic(path, my_list):
    """
    Функция save_list_to_file_classic.

    Принимает 2 аргумента:
    строка (название файла или полный путь к файлу),
    список (для сохранения).

    Сохраняет список в файл. Проверить, что записалось в файл (прочитать файл).
    """

    with open(path, 'w+') as file:
        file.write(str(my_list))


if __name__ == '__main__':
    file_path = 'task1.txt'
    save_list_to_file_classic(file_path,
                              [1, 2, 3, 4])

    with open(file_path, 'r') as f:
        print(f.read())

    # ----------------------------
    # из одного файла читаем в другой пишем
    with open(file_path, 'r') as f1, open('task2.txt', 'w') as f2:
        f2.write(f1.read())
