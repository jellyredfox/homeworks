"""
Класс Human.

Поля:
age,
first_name,
last_name.

При создании экземпляра инициализировать поля класса.

Создать метод get_age, который возвращает возраст человека.

Перегрузить оператор __eq__, который сравнивает объект человека с другим по атрибутам.

Перегрузить оператор __str__, который возвращает строку в виде "Имя: first_name last_name Возраст: age".
"""


class Human:
    def __init__(self, age, first_name, last_name):
        self.age = age
        self.first_name = first_name
        self.last_name = last_name

    def get_age(self):
        return self.age

    def __eq__(self, other):
        return self.age == other.age and self.first_name == other.first_name\
               and self.last_name == other.last_name

    def __str__(self):
        return f"Имя: {self.first_name} {self.last_name} Возраст: {str(self.age)}"
