"""
Класс Worker.

Поля:
имя: name,
зарплата: salary,
должность: position.

Методы:
__gt__: возвращает результат сравнения (>) зарплат работников.
__len__: возвращает количетсво букв в названии должности.
"""


class Worker:
    def __init__(self, name, salary, position):
        self.name = name
        self.salary = salary
        self.position = position

    def __gt__(self, other):
        return self.salary > other.salary

    def __len__(self):
        return len(self.position)


if __name__ == '__main__':
    john = Worker('John', 1000, 'janitor')
    mike = Worker('Mike', 2000, 'security')
    pete = Worker('Pete', 5000, 'teacher')
    fil = Worker('Fil', 5500, 'teacher')
    sue = Worker('Sue', 10000, 'director')