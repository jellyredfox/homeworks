"""
Класс School.

Поля:
список людей в школе (общий list для Pupil и Worker): people,
номер школы: number.

Методы:
get_avg_mark: вернуть средний балл всех учеников школы
get_avg_salary: вернуть среднюю зп работников школы
get_worker_count: вернуть сколько всего работников в школе
get_pupil_count: вернуть сколько всего учеников в школе
get_pupil_names: вернуть все имена учеников (с повторами, если есть)
get_unique_worker_positions: вернуть список всех должностей работников (без повторов, подсказка: set)
get_max_pupil_age: вернуть возраст самого старшего ученика
get_min_worker_salary: вернуть самую маленькую зп работника
get_min_salary_worker_names: вернуть список имен работников с самой маленькой зп
(список из одного или нескольких элементов)
"""

from topic_7_oop.hw.class_4_1_pupil import Pupil
from topic_7_oop.hw.class_4_2_worker import Worker


class School:
    def __init__(self, people, number):
        self.people = people
        self.number = number

    def get_avg_mark(self):
        return sum([p.get_avg_mark() for p in self.people if type(p) == Pupil]) / self.get_pupil_count()
        # средний балл всех учеников школы

    def get_avg_salary(self):
        return sum([w.salary for w in self.people if type(w) == Worker]) / self.get_worker_count()
        # среднюю зп работников школы

    def get_worker_count(self):
        return len([w for w in self.people if type(w) == Worker])  # сколько всего работников в школе

    def get_pupil_count(self):
        return len([p for p in self.people if type(p) == Pupil])  # сколько всего учеников в школе

    def get_pupil_names(self):
        return [p.name for p in self.people if type(p) == Pupil]  # все имена учеников (с повторами, если есть)

    def get_unique_worker_positions(self):
        return set([w.position for w in self.people if type(w) == Worker])
        # список всех должностей работников (без повторов, подсказка: set)

    def get_max_pupil_age(self):
        return max([p.age for p in self.people if type(p) == Pupil])  # возраст самого старшего ученика

    def get_min_worker_salary(self):
        return min([w.salary for w in self.people if type(w) == Worker])  # самую маленькую зп работника

    def get_min_salary_worker_names(self):
        return [w.name for w in self.people if type(w) == Worker and w.salary == self.get_min_worker_salary()]
        # список имен работников с самой маленькой зп (список из одного или нескольких элементов)


if __name__ == '__main__':
    pupil_1 = Pupil('Петя',
                    15,
                    {
                        'math': [3, 3, 3],
                        'history': [4, 4, 4],
                        'english': [5, 5, 5]
                    })
    pupil_2 = Pupil('Маша',
                    15,
                    {
                        'math': [4, 4, 4],
                        'history': [4, 4, 4],
                        'english': [4, 4, 4]
                    })

    pupil_3 = Pupil('Маша',
                    13,
                    {
                        'math': [5, 5, 5],
                        'history': [5, 5, 5],
                        'english': [5, 5, 5]
                    })

    worker_1 = Worker('Миша',
                      86324.50,
                      "Учитель математики")

    worker_2 = Worker('Лена',
                      56324.50,
                      "Повар")

    worker_3 = Worker('Василиса',
                      56324.50,
                      "Учитель математики")

    school = School([
        pupil_1,
        pupil_2,
        pupil_3,

        worker_1,
        worker_2,
        worker_3
    ], 555
    )
    print(school.get_avg_mark())
    print(school.get_min_salary_worker_names())
    print(school.get_unique_worker_positions())
    print(school.get_pupil_names())
    print(school.get_min_worker_salary())
    print(school.get_max_pupil_age())
    print(school.get_pupil_count())
    print(school.get_worker_count())
    print(school.get_min_salary_worker_names())
    print(school.get_avg_salary())
