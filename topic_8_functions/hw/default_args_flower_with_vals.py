"""
Функция flower_with_default_vals.

Принимает 3 аргумента:
цветок (по умолчанию "ромашка"),
цвет (по умолчанию "белый"),
цена (по умолчанию 10.25).

Функция flower_with_default_vals выводит строку в формате
"Цветок: <цветок> | Цвет: <цвет> | Цена: <цена>".

При этом в функции flower_with_default_vals есть проверка на то, что цветок и цвет - это строки

(* Подсказка: type(x) == str или isinstance(s, str)), а также цена - это число больше 0, но меньше 10000.
В функции main вызвать функцию flower_with_default_vals различными способами
(перебрать варианты: цветок, цвет, цена, цветок цвет, цветок цена, цвет цена, цветок цвет цена).

(* Использовать именованные аргументы).
"""


def flower_with_default_vals(flower="ромашка", color="белый", price=10.25):
    if not isinstance(flower, str) or not isinstance(color, str):
        return 'Must be string!'
    elif 0 > price or price > 10000:
        return 'Bad price!'
    print("Цветок: {} | Цвет: {} | Цена: {}".format(flower, color, price))


if __name__ == '__main__':
    flower_with_default_vals(flower='ромашка', color='белый', price=10.25)
    flower_with_default_vals(flower='гвоздика', color='красный')
    flower_with_default_vals(flower='роза', price=12.36)
    flower_with_default_vals(color='синий', price=17.41)
    flower_with_default_vals(flower='альстромерия', color='желтый', price=8.16)
    flower_with_default_vals()
